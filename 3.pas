program e3;

uses crt;

var a,b,c,delta: Real;

begin
	readln(a);
	readln(b);
	readln(c);
	delta := b*b - 4*a*c;
	if delta = 0 then
	begin
		writeln( (-b / 2*a) :4:4);
	end;
	if delta > 0 then
	begin
		writeln( (-b+sqrt(delta)/2*a) :4:4);
		writeln( (-b-sqrt(delta)/2*a) :4:4);
	end;
	if delta < 0 then
	begin
		writeln('sem result');
	end;
end.