program e12;

type Lista = Array of Real;

procedure writeLista(l: Lista);
var i: Integer;
begin
	for i:=0 to Length(l) do
		writeln(l[i]:8:8);
end;

function addict(x: Lista;e: Real): Lista;
var l,i: Integer;
var temp: Lista;
begin
	l := Length(x);
	SetLength(temp,l+1);
	for i := 0 to l do
	begin
		temp[i] := x[i];
	end;
	temp[l+1] := e;
	addict := temp;
end;

var abc: Lista;
var i: Integer;

begin
	SetLength(abc,5);
	for i:= 0 to 4 do
		abc[i] := (i*2)/3;
	writeLista(abc);
	abc := addict(abc,1.3);
	writeln();
	writeLista(abc);
end.