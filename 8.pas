program e8;

Uses math;

var y: Integer;

function factorial(f: Integer): Integer;
begin
	if f =1 then factorial := 1
	else factorial := f*factorial(f-1);
end;

function taylor(x,n: Integer): Real;
var temp: Real;
var i: Integer;
begin
	temp := 1.0;
	for i:= 1 to n do
	begin
		temp := temp + (intpower(x,i)/factorial(i));
	end;
	taylor := temp;
end;

begin
	write('n-ésimo termo: ');
	readln(y);
	writeln(taylor(3,y):8:8);
end.