program e7;

var dolar: Array of Real;
var n,count: Integer;

begin
	write('Define n: ');
	readln(n);
	SetLength(dolar,n);
	dolar[0] := 100.00;
	for count := 1 to n do
		dolar[count] := dolar[count-1] + (dolar[count-1]*0.05);
	for count := 0 to n do
		writeln(dolar[count]:8:8);
end.