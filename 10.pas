program e10;

type Intervalo = Array[1..2] of Integer;

var n,i: Integer;
var whatever: Intervalo;

procedure divisor(a: Intervalo;b: Integer);
begin
	for i := a[1] to a[2] do
	begin
		if n mod i = 0 then writeln(i);
	end;
end;

begin
	write('inicio intervalo: ');
	readln(whatever[1]);
	write('final intervalo: ');
	readln(whatever[2]);
	write('n: ');
	readln(n);
	writeln('os inteiros divisores: ');
	divisor(whatever,n);
end.