program e4;

var a,b: Array[1..5] of Real;
var i: Integer;

begin
	for i:= 1 to 5 do
	begin
		a[i] := i+20/2;
		writeln(a[i]:2:2);
	end;
	writeln();
	writeln();
	for i:= 1 to 5 do
		b[6-i] := a[i];
	for i:= 1 to 5 do
		writeln(b[i]:2:2);
end.