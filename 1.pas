program exec1;

var x,y: Real;

begin
	read(x);
	y := x * x * x;
	writeln(y:8:4);
end.