program e11;

type Lista = Array of Real;

procedure writeLista(l: Lista);
var i: Integer;
begin
	for i:=0 to Length(l) do
		writeln(l[i]:8:8);
end;

function addict(e: Real;x: Lista): Lista;
var i,l: Integer;
var temp: Lista;
begin
	l := Length(x);
	SetLength(temp,l+1);
	for i := 0 to l do
	begin
		temp[i+1] := x[i];
	end;
	temp[0] := e;
	addict := temp;
end;

var abc: Lista;
var i: Integer;

begin
	SetLength(abc,5);
	for i:= 0 to 4 do
		abc[i] := (i*2)/3;
	writeLista(abc);
	abc := addict(1.3,abc);
	writeln();
	writeLista(abc);
end.